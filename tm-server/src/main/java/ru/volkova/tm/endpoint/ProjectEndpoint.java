package ru.volkova.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.endpoint.IProjectEndpoint;
import ru.volkova.tm.api.service.IProjectService;
import ru.volkova.tm.api.service.ServiceLocator;
import ru.volkova.tm.entity.Project;
import ru.volkova.tm.entity.Session;
import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.exception.auth.AccessDeniedException;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@WebService
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @SneakyThrows
    @NotNull
    @WebMethod
    public Project changeProjectOneStatusById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "status", partName = "status") Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        if (!serviceLocator.getProjectService()
                .changeOneStatusById(session.getUserId(), id, status).isPresent())
            throw new ObjectNotFoundException();
        return serviceLocator.getProjectService()
                .changeOneStatusById(session.getUserId(), id, status).get();

    }

    @NotNull
    @WebMethod
    public Project changeProjectStatusByIndex(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index,
            @Nullable @WebParam(name = "status", partName = "status") Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        if (!serviceLocator.getProjectService()
                .changeOneStatusByIndex(userId, index, status).isPresent())
            throw new ObjectNotFoundException();
        return serviceLocator.getProjectService()
                .changeOneStatusByIndex(userId, index, status).get();
    }

    @SneakyThrows
    @NotNull
    @WebMethod
    public Project changeProjectStatusByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "status", partName = "status") Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        if (!serviceLocator.getProjectService()
                .changeOneStatusByName(session.getUserId(), name, status).isPresent())
            throw new ObjectNotFoundException();
        return serviceLocator.getProjectService()
                .changeOneStatusByName(session.getUserId(), name, status).get();
    }

    @WebMethod
    public void clearProject(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        serviceLocator.getProjectService().clear(userId);
    }

    @SneakyThrows
    @NotNull
    @WebMethod
    public List<Project> findAllProjects(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        return serviceLocator.getProjectService().findAll(userId);
    }

    @NotNull
    @WebMethod
    public Project findProjectById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "id", partName = "id") String id
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        if (!serviceLocator.getProjectService().findById(userId, id).isPresent())
            throw new ObjectNotFoundException();
        return serviceLocator.getProjectService().findById(userId, id).get();
    }

    @NotNull
    @WebMethod
    public Project findProjectByIndex(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        if (!serviceLocator.getProjectService().findOneByIndex(userId, index).isPresent())
            throw new ObjectNotFoundException();
        return serviceLocator.getProjectService().findOneByIndex(userId, index).get();
    }

    @NotNull
    @WebMethod
    public Project findProjectByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        if (!serviceLocator.getProjectService().findOneByName(userId, name).isPresent())
            throw new ObjectNotFoundException();
        return serviceLocator.getProjectService().findOneByName(userId, name).get();
    }

    @NotNull
    @WebMethod
    public Project finishProjectById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        if (!serviceLocator.getProjectService().finishOneById(userId, id).isPresent())
            throw new ObjectNotFoundException();
        return serviceLocator.getProjectService().finishOneById(userId, id).get();
    }

    @NotNull
    @WebMethod
    public Project finishProjectByIndex(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        if (!serviceLocator.getProjectService().finishOneByIndex(userId, index).isPresent())
            throw new ObjectNotFoundException();
        return serviceLocator.getProjectService().finishOneByIndex(userId, index).get();
    }

    @NotNull
    @WebMethod
    public Project finishProjectByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        if (!serviceLocator.getProjectService().finishOneByName(userId, name).isPresent())
            throw new ObjectNotFoundException();
        return serviceLocator.getProjectService().finishOneByName(userId, name).get();
    }

    @WebMethod
    public void removeProjectById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "id", partName = "id") String id
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        serviceLocator.getProjectService().removeById(userId, id);
    }

    @WebMethod
    public void removeProjectByIndex(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        serviceLocator.getProjectService().removeOneByIndex(userId, index);
    }

    @WebMethod
    public void removeProjectByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        serviceLocator.getProjectService().removeOneByName(userId, name);
    }

    @NotNull
    @WebMethod
    public Project startProjectById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        if (!serviceLocator.getProjectService().startOneById(userId, id).isPresent())
            throw new ObjectNotFoundException();
        return serviceLocator.getProjectService().startOneById(userId, id).get();
    }

    @NotNull
    @WebMethod
    public Project startProjectByIndex(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        if (!serviceLocator.getProjectService().startOneByIndex(userId, index).isPresent())
            throw new ObjectNotFoundException();
        return serviceLocator.getProjectService().startOneByIndex(userId, index).get();
    }

    @NotNull
    @WebMethod
    public Project startProjectByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        if (!serviceLocator.getProjectService().startOneByName(userId, name).isPresent())
            throw new ObjectNotFoundException();
        return serviceLocator.getProjectService().startOneByName(userId, name).get();
    }

    @NotNull
    @WebMethod
    public Project updateProjectById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        if (!serviceLocator.getProjectService()
                .updateOneById(userId, id, name, description).isPresent())
            throw new ObjectNotFoundException();
        return serviceLocator.getProjectService()
                .updateOneById(userId, id, name, description).get();
    }

    @NotNull
    @WebMethod
    public Project updateProjectByIndex(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        if (!serviceLocator.getProjectService()
                .updateOneByIndex(userId, index, name, description).isPresent())
            throw new ObjectNotFoundException();
        return serviceLocator.getProjectService()
                .updateOneByIndex(userId, index, name, description).get();
    }

    @NotNull
    @WebMethod
    public Project addProjectByUser(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        if (!serviceLocator.getProjectService().add(userId, name, description).isPresent())
            throw new ObjectNotFoundException();
        return serviceLocator.getProjectService().add(userId, name, description).get();
    }

}
