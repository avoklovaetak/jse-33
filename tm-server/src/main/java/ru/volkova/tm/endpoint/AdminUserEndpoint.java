package ru.volkova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.endpoint.IAdminUserEndpoint;
import ru.volkova.tm.api.service.ServiceLocator;
import ru.volkova.tm.entity.Session;
import ru.volkova.tm.entity.User;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.exception.auth.AccessDeniedException;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.Optional;

@WebService
public class AdminUserEndpoint extends AbstractEndpoint implements IAdminUserEndpoint {

    public AdminUserEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @WebMethod
    public User addUser(
            @Nullable @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "entity", partName = "entity") User entity
    ) {
        serviceLocator.getSessionService().validate(session);
        if (!serviceLocator.getAdminUserService().add(entity).isPresent()) throw new ObjectNotFoundException();
        return serviceLocator.getAdminUserService().add(entity).get();
    }

    @WebMethod
    public void addAllUsers(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "entities", partName = "entities") List<User> entities
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getAdminUserService().addAll(entities);
    }

    @WebMethod
    public void clearUsers(@NotNull @WebParam(name = "session", partName = "session") Session session) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getAdminUserService().clear();
    }

    @NotNull
    @WebMethod
    public User createUserByLogPass(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login,
            @Nullable @WebParam(name = "password", partName = "password") String password
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getAdminUserService().createUser(login, password);
    }

    @NotNull
    @WebMethod
    public User createUserWithEmail(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login,
            @Nullable @WebParam(name = "password", partName = "password") String password,
            @Nullable @WebParam(name = "email", partName = "email") String email
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getAdminUserService().createUserWithEmail(login, password, email);
    }

    @NotNull
    @WebMethod
    public User createUserWithRole(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login,
            @Nullable @WebParam(name = "password", partName = "password") String password,
            @Nullable @WebParam(name = "role", partName = "role") Role role
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getAdminUserService().createUserWithRole(login, password, role);
    }

    @NotNull
    @WebMethod
    public List<User> findAllUser(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getAdminUserService().findAll();
    }

    @NotNull
    @WebMethod
    public User findUserById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) {
        serviceLocator.getSessionService().validate(session);
        if (!serviceLocator.getAdminUserService().findById(id).isPresent()) throw new ObjectNotFoundException();
        return serviceLocator.getAdminUserService().findById(id).get();
    }

    @NotNull
    @WebMethod
    public User findUserByLogin(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) {
        serviceLocator.getSessionService().validate(session);
        if (!serviceLocator.getAdminUserService().findByLogin(login).isPresent()) throw new ObjectNotFoundException();
        return serviceLocator.getAdminUserService().findByLogin(login).get();
    }

    @WebMethod
    public boolean isEmailExists(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "email", partName = "email") String email
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getAdminUserService().isEmailExists(email);
    }

    @WebMethod
    public boolean isLoginExists(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getAdminUserService().isLoginExists(login);
    }

    @WebMethod
    public void lockByEmail(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "email", partName = "email") String email
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getAdminUserService().lockByEmail(email);
    }

    @WebMethod
    public void lockById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getAdminUserService().lockById(id);
    }

    @WebMethod
    public void lockByLogin(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getAdminUserService().lockByLogin(login);
    }

    @WebMethod
    public void removeUser(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "user", partName = "login") User user
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getAdminUserService().remove(user);
    }

    @WebMethod
    public void removeUserByEmail(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "email", partName = "email") String email
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getAdminUserService().removeByEmail(email);
    }

    @WebMethod
    public void removeUserById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getAdminUserService().removeById(id);
    }

    @WebMethod
    public void removeUserByLogin(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getAdminUserService().removeByLogin(login);
    }


    @WebMethod
    public void unlockUserByEmail(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "email", partName = "email") String email
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getAdminUserService().unlockByEmail(email);
    }

    @WebMethod
    public void unlockUserById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getAdminUserService().unlockById(id);
    }

    @WebMethod
    public void unlockUserByLogin(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getAdminUserService().unlockByLogin(login);
    }

    @WebMethod
    @NotNull
    public User updateUser(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "firstName", partName = "firstName") String firstName,
            @Nullable @WebParam(name = "secondName", partName = "secondName") String secondName,
            @Nullable @WebParam(name = "middleName", partName = "middleName") String middleName
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        if (!serviceLocator.getAdminUserService()
                .updateUser(userId, firstName, secondName, middleName).isPresent())
            throw new ObjectNotFoundException();
        return serviceLocator.getAdminUserService()
                .updateUser(userId, firstName, secondName, middleName).get();
    }

}
