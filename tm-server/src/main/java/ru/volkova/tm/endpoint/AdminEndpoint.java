package ru.volkova.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.endpoint.IAdminEndpoint;
import ru.volkova.tm.api.service.ServiceLocator;
import ru.volkova.tm.entity.Session;
import ru.volkova.tm.exception.auth.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    public AdminEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    public void DataBinarySave(
            @Nullable @WebParam(name = "session", partName = "session") Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getAdminService().DataBinarySave();
    }

    @WebMethod
    public void DataBinaryLoad(
            @Nullable @WebParam(name = "session", partName = "session") Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getAdminService().DataBinaryLoad();
    }

    @WebMethod
    public void DataJsonLoad(
            @Nullable @WebParam(name = "session", partName = "session") Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getAdminService().DataJsonLoad();
    }

    @WebMethod
    public void DataJsonSave(
            @Nullable @WebParam(name = "session", partName = "session") Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getAdminService().DataJsonSave();
    }

    @WebMethod
    public void DataBase64Load(
            @Nullable @WebParam(name = "session", partName = "session") Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getAdminService().DataBase64Load();
    }

    @WebMethod
    public void DataBase64Save(
            @Nullable @WebParam(name = "session", partName = "session") Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getAdminService().DataBase64Save();
    }

    @WebMethod
    public void DataJsonLoadFasterxml(
            @Nullable @WebParam(name = "session", partName = "session") Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getAdminService().DataJsonLoadFasterxml();
    }

    @WebMethod
    public void DataJsonSaveFasterxml(
            @Nullable @WebParam(name = "session", partName = "session") Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getAdminService().DataJsonSaveFasterxml();
    }

    @WebMethod
    public void DataJsonLoadJaxb(
            @Nullable @WebParam(name = "session", partName = "session") Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getAdminService().DataJsonLoadJaxb();
    }

    @WebMethod
    public void DataJsonSaveJaxb(
            @Nullable @WebParam(name = "session", partName = "session") Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getAdminService().DataJsonSaveJaxb();
    }

    @WebMethod
    public void DataXmlLoadFasterxml(
            @Nullable @WebParam(name = "session", partName = "session") Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getAdminService().DataXmlLoadFasterxml();
    }

    @WebMethod
    public void DataXmlSaveFasterxml(
            @Nullable @WebParam(name = "session", partName = "session") Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getAdminService().DataXmlSaveFasterxml();
    }

    @WebMethod
    public void DataXmlLoadJaxb(
            @Nullable @WebParam(name = "session", partName = "session") Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getAdminService().DataXmlLoadJaxb();
    }

    @WebMethod
    public void DataXmlSaveJaxb(
            @Nullable @WebParam(name = "session", partName = "session") Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getAdminService().DataXmlSaveJaxb();
    }

    @WebMethod
    public void DataYamlLoadFasterxml(
            @Nullable @WebParam(name = "session", partName = "session") Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getAdminService().DataYamlLoadFasterxml();
    }

    @WebMethod
    public void DataYamlSaveFasterxml(
            @Nullable @WebParam(name = "session", partName = "session") Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getAdminService().DataYamlSaveFasterxml();
    }

}
