package ru.volkova.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IAdminEndpoint {

    @WebMethod
    void DataBinarySave(
            @Nullable @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void DataBinaryLoad(
            @Nullable @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void DataJsonLoad(
            @Nullable @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void DataJsonSave(
            @Nullable @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void DataBase64Load(
            @Nullable @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void DataBase64Save(
            @Nullable @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void DataJsonLoadFasterxml(
            @Nullable @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void DataJsonSaveFasterxml(
            @Nullable @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void DataJsonLoadJaxb(
            @Nullable @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void DataJsonSaveJaxb(
            @Nullable @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void DataXmlLoadFasterxml(
            @Nullable @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void DataXmlSaveFasterxml(
            @Nullable @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void DataXmlLoadJaxb(
            @Nullable @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void DataXmlSaveJaxb(
            @Nullable @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void DataYamlLoadFasterxml(
            @Nullable @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void DataYamlSaveFasterxml(
            @Nullable @WebParam(name = "session", partName = "session") Session session
    );

}
