package ru.volkova.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.entity.Project;
import ru.volkova.tm.entity.Session;
import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.exception.auth.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;
import java.util.Optional;

public interface IProjectEndpoint {

    @SneakyThrows
    @NotNull
    @WebMethod
    Project changeProjectOneStatusById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "status", partName = "status") Status status
    );

    @NotNull
    @WebMethod
    Project changeProjectStatusByIndex(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index,
            @Nullable @WebParam(name = "status", partName = "status") Status status
    );

    @SneakyThrows
    @NotNull
    @WebMethod
    Project changeProjectStatusByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "status", partName = "status") Status status
    );

    @WebMethod
    public void clearProject(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    );

    @SneakyThrows
    @NotNull
    @WebMethod
    List<Project> findAllProjects(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    );

    @NotNull
    @WebMethod
    Project findProjectById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "id", partName = "id") String id
    );

    @NotNull
    @WebMethod
    Project findProjectByIndex(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index
    );

    @NotNull
    @WebMethod
    Project findProjectByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    );

    @NotNull
    @WebMethod
    Project finishProjectById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    );

    @NotNull
    @WebMethod
    Project finishProjectByIndex(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index
    );

    @NotNull
    @WebMethod
    Project finishProjectByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    );

    @WebMethod
    void removeProjectById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "id", partName = "id") String id
    );

    @WebMethod
    void removeProjectByIndex(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index
    );

    @WebMethod
    void removeProjectByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    );

    @NotNull
    @WebMethod
    Project startProjectById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    );

    @NotNull
    @WebMethod
    Project startProjectByIndex(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index
    );

    @NotNull
    @WebMethod
    Project startProjectByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    );

    @NotNull
    @WebMethod
    Project updateProjectById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    );

    @NotNull
    @WebMethod
    Project updateProjectByIndex(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    );

    @NotNull
    @WebMethod
    Project addProjectByUser(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    );

}
