package ru.volkova.tm.repository;

import ru.volkova.tm.api.repository.ISessionRepository;
import ru.volkova.tm.entity.Session;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {
}
