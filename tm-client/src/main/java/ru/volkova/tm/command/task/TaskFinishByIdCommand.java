package ru.volkova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.endpoint.Session;
import ru.volkova.tm.endpoint.Role;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.util.TerminalUtil;

public class TaskFinishByIdCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "finish task by id";
    }

    @Override
    public void execute() {
        if (bootstrap == null) throw new ObjectNotFoundException();
        if (endpointLocator == null) throw new ObjectNotFoundException();
        @Nullable final Session session = bootstrap.getSession();
        System.out.println("[FINISH TASK]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        endpointLocator.getTaskEndpoint().finishTaskById(session, id);
    }

    @NotNull
    @Override
    public String name() {
        return "task-finish-by-id";
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}